
fetch(`https://pokeapi.co/api/v2/pokemon/?offset=0&limit=150`)
    .then((respuestaDelApi) => {
        return respuestaDelApi.json()
    })
    .then((respuestaEnJson) => {

        return respuestaEnJson.results;

    })
    .then((resultados) => {
        return resultados.map((pokemon) => pokemon.url);

    })
    .then((urlPokemon) => { 
       urlPokemon.forEach(async url => {
            const response = await fetch(`${url}`)  ;
           const alfinpokemons = await response.json();
        const pokemon = {  
            name: alfinpokemons.name,
            image: alfinpokemons.sprites['front_default'],
            type: alfinpokemons.types.map((type) => type.type.name).join(', '),
            id: alfinpokemons.id
        }
        
           pintarpokemon (pokemon)
          //console.log(pokemon) 
        })});

     const pintarpokemon = (result) => 
     {
       
        
        const nodeli = document.createElement("LI");  
        nodeli.className= "card"
        const pokedex = document.getElementById("pokedex")   
        pokedex.appendChild(nodeli);
     
       
        const nodeP = document.createElement("p");   
        nodeP.className = "card-title";
        nodeP.textContent= `${result.name}`
        nodeli.appendChild(nodeP);  

        const nodeimg = document.createElement("img");   
        nodeimg.className = "card-image";
        nodeimg.setAttribute('src', `${result.image}`)   
        nodeli.appendChild(nodeimg);   

        const nodePsub = document.createElement("p");   
        nodePsub.className = "card-subtitle";
        nodePsub.textContent= `Tipo del Pokemon: ${result.type}`
        nodeli.appendChild(nodePsub);




}
